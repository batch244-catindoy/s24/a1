const number = 3;
const getCube = Math.pow(3, 3);

result = `The cube of ${number} is ${getCube}`;
console.log(result);
console.log(getCube);

const completeAddress = ["San Roque", "Rosario", "Batangas"];
const [street, barangay, city] = completeAddress

console.log(`I live at ${street} ${barangay} ${city}`);

const animal = {
	type: "Cat",
	character: "Long-Haired",
	kind: "Persian"

};

const {type, character, kind} = animal;
console.log(`The ${kind} ${type} is ${character} breed of ${type}.`)

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) =>{
	console.log(number);
	
})
let reduceNumber = numbers.reduce((x, y) => x + y)
	console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let myDog = new Dog()

myDog.name = "Kylie";
myDog.age = 2;
myDog.breed = "Pomeranian";
console.log(myDog);

let myNewDog = new Dog ("Elsa", 8, "Husky");
console.log(myNewDog);


